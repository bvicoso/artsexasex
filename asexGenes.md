# Call genes differentially expressed between sexual and asexual lineages

This R code is very similar to the one for calling sex-biased genes. All parameters set in that script are also valid here. For further details, see [here](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/SB_SexAsex.md).

Pick some parameters: tissue,  species to use for calling sex-biased genes (and as proxy for ancestral expression), and focal species (the asexual or control species whose expression we are interested in):
```
#R version 3.6.1 (2019-07-05)

##########Pick a few parameters
###Working directory
setwd("/home/ahuylman/1_Projects/Artemia/Scripts/Revision/")
###tissue to use (Head, Gonad or Tho)
tissue<-"Gonad"
### Pick pair of species to analyse
#Species for calling sex-biased genes (Asin, Akaz or AurmSexual) and ancestral gene expression
ancestral<-"Akaz"
#Now pick the focal species "asexual"
#Options: Asin, Akaz, AurmSexual, Apar, Aata, AurmAsex
focal<-"AurmSexual"
###Samples to exclude
bad<-c("Apar_F_Gonad.3", "Apar_F_Gonad.2", "Apar_F_Head.3", "Apar_F_Head.2", "Apar_F_Tho.2", "Apar_F_Tho.3")
```

Load data:
```
#######load data and sample information
Asex2Asin <- read.table("/home/ahuylman/1_Projects/Artemia/DifferentialGeneExpression/Asex2Asin/AsexVsSex/results.txt", sep="\t", header=TRUE, row.names=1, na.strings="NA")

# filter out transcripts <= 500 bp
exp <- Asex2Asin[(Asex2Asin$len > 500),]

info<-read.table("SampleInfo.txt", sep=" ", head=F)


colnames(info)<-c("code", "Sample", "Type")
exp2<-exp[grep("count", colnames(exp))]
#rownames(exp2) <- exp[,1]
colClean <- function(x){ colnames(x) <- gsub("\\.count", "", colnames(x)); x } 
exp2 <- colClean(exp2)
#transpose
exp3<-t(exp2)
head(info,2)
#add sample information
full<-merge(info, exp3, by.x="code", by.y="row.names")[,-1]
row.names(full)<-make.names(full[,1], unique=TRUE)
full<-full[,-1]
head(full[,1:5])
```

Filter out bad samples (specified at the beginning) and use only the species specified as ancestral (beginning) and the asexual lineages:
```
#################### Asexuality Genes ###############################
final_asex<-full[grep(tissue, rownames(full)),][,2:ncol(full)]
#Keep only Species of interest (ancestral females & asexuals)
final_asex<-final_asex[grep(paste(ancestral,"_F|Apar|AurmAsex|Aata", sep=""), rownames(final_asex)),]
#Remove outlier samples that we stored in vector "bad"
final_asex<-(subset(final_asex, !(rownames(final_asex) %in% bad)))
```

Run DESeq2 differential expression between sexual females and asexuals:
```
######sort so samples are in alphabetical order
final_asex<-final_asex[ order(row.names(final_asex)), ]
#transpose to right format
count_matrix_asex<-t(final_asex)

######Make table with sample information
samples_asex<-as.data.frame(colnames(count_matrix_asex))
rownames(samples_asex)<-colnames(count_matrix_asex)
sexuals<-as.data.frame(samples_asex[grep(ancestral, rownames(samples_asex)),])
colnames(sexuals)<-"Samplename"
sexuals$Status<-"Sex"
asexuals<-as.data.frame(samples_asex[-grep(paste("^",ancestral, sep=""), rownames(samples_asex)),])
colnames(asexuals)<-"Samplename"
asexuals$Status<-"Asex"
sampletable_asex<-rbind(sexuals, asexuals)
sampletable_asex$Species <- as.factor(substring(sampletable_asex$Samplename, 0, regexpr("_", sampletable_asex$Samplename) -1))
rownames(sampletable_asex)<-sampletable_asex$Samplename
sampletable_asex<-sampletable_asex[ order(row.names(sampletable_asex)), ]

########create the DESeq object
library("DESeq2")
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data
se_star_matrix_asex <- DESeqDataSetFromMatrix(countData = count_matrix_asex, colData = sampletable_asex, design = ~ Status)

dds_asex <- DESeq(se_star_matrix_asex)
resultsNames(dds_asex) # lists the coefficients, so you know what to write for "coef=" in the next command

#####Standard differential expression analysis
asex_genes <- results(dds_asex, name="Status_Sex_vs_Asex", pAdjustMethod = "BH", alpha=0.05)
```

Show results based on adjusted P-values for combined analysis (all asexuals versus the sexual females):
```
### Results with adjusted P-value cut-off
summary(asex_genes)
```

Show results based on log2 fold-change (in the case larger 2) for combined analysis (all asexuals versus the sexual females):
```
### Results with fold-change cut-off
resSign_asex <- subset(asex_genes, abs(log2FoldChange) > 1)
resOrdered_asex <- resSign_asex[order(resSign_asex$log2FoldChange),]
```

Show results based on adjusted P-values for individual analysis (each asexual lineage versus the sexual females):
```
##### individual species comparisons
se_star_matrix_spec <- DESeqDataSetFromMatrix(countData = count_matrix_asex, colData=sampletable_asex, design=~Species)
dds_spec <- DESeq(se_star_matrix_spec)

species_list <- c("Apar", "Aata", "AurmAsex")

library(tidyverse)
DESeq2_indv_results <- lapply(species_list, function(x) {
  summary(results(dds_spec, alpha=0.05, contrast=c("Species", x, ancestral)))})
```
