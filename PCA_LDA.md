# Use PCA - linear discriminant analysis (PCA-LDA) to compute maleness score of asexuals

## 1. Outline of the approach 

The gene expression data (in RPKM) was filtered to keep only transcripts with expression >1 in all samples of the tissue of interest,  and normalized with NormalyzerDE (NormalyzerDE_1.4.0). We obtained principal components from the gene expression table with the R function "prcomp", and ran a linear discriminant analysis on a subset of these principal components using the R package MASS (MASS_7.3-51.6). Some sexual individuals (80%) were used as the training set to infer the linear discriminant model (randomly assigned with the R package caret, caret_6.0-86), while the other two individuals were used to make predictions and confirm that the model was working. Finally the same model was applied to predict the sex of asexual individuals. 

The first 5 and 7 principal components were included in the gonad and head linear discriminant analyses, respectively. These numbers were chosen as they corresponded to the minimum set of principal components required to fully separate males from sexual females. 

The following samples were excluded from the analysis, as their distribution was not aligned with the others post-normalization and/or they were outliers in a clustering analysis:
* Aibi replicates that were originally sequenced and had issues ("Aib_F_Gonad.3", "Aib_F_Gonad.2", "Aib_F_Head.3", "Aib_F_Head.2", "Aib_F_Tho.2", "Aib_F_Tho.3")
* One of the A. kazakhstan male head replicates, which had a strange distribution of expression values after normalization ("Kaz_M_Head"). Adding this sample does not change any of the conclusions (the model classifies all males as males, as females as females, and all but one asexual samples as females), but this data point shows up as a clear outlier.

## 2. Results

The output of the analysis is a single value (LG1) for each individual, that captures the most differentiation between males and females in the training set. 

## 3. Packages needed

We use MASS and caret, and NormalyzerDE for normalization. 

```
install.packages('MASS')
install.packages('caret')
install.packages('fmsb') #remove if I don't use VIF in the end

if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("NormalyzerDE")
```


## 4. Data

Let's get the original counts from [this file](https://seafile.ist.ac.at/f/181d9e97950543ec87a6/?dl=1).

And the sample descriptions from [sampleinfo.txt](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/data/SampleInfo.txt).

The file looks like (space-separated):

 ID |  Species/Tissue | Sex 
---|---|---
X101424 | Aib_F_Head | F
X101425 | Aib_F_Head | F
X101426 | Aib_F_Tho | F
X101427 | Aib_F_Tho | F

NB: the script replaces the original sample codes with different names. To get the correspondence, type (after running the LDA script):
```
full<-merge(info, exp3, by.x="code", by.y="row.names")
row.names(full)<-make.names(full[,2], unique=TRUE)
(full[,1:2])
```
<details>
  <summary>or click below!</summary>
  

New | original | info 
---|---|---
Aib_F_Head | X101424 | Aib_F_Head
Aib_F_Head.1 | X101425 | Aib_F_Head
Aib_F_Tho | X101426 | Aib_F_Tho
Aib_F_Tho.1 | X101427 | Aib_F_Tho
Aib_F_Gonad | X101428 | Aib_F_Gonad
Aib_F_Gonad.1 | X101429 | Aib_F_Gonad
Aib_F_WB | X101440 | Aib_F_WB
Aib_F_WB.1 | X101441 | Aib_F_WB
Sin_M_WB | X39869 | Sin_M_WB
Sin_M_WB.1 | X39870 | Sin_M_WB
Sin_F_WB | X39871 | Sin_F_WB
Sin_F_WB.1 | X39872 | Sin_F_WB
Kaz_M_WB | X39873 | Kaz_M_WB
Kaz_M_WB.1 | X39874 | Kaz_M_WB
Kaz_F_WB | X39875 | Kaz_F_WB
Kaz_F_WB.1 | X39876 | Kaz_F_WB
Sin_M_Head | X39877 | Sin_M_Head
Sin_M_Head.1 | X39878 | Sin_M_Head
Sin_M_Tho | X39879 | Sin_M_Tho
Sin_M_Tho.1 | X39880 | Sin_M_Tho
Sin_M_Gonad | X39895 | Sin_M_Gonad
Sin_M_Gonad.1 | X39896 | Sin_M_Gonad
Sin_F_Head | X39897 | Sin_F_Head
Sin_F_Head.1 | X39898 | Sin_F_Head
Sin_F_Gonad | X39899 | Sin_F_Gonad
Sin_F_Gonad.1 | X39900 | Sin_F_Gonad
Sin_M_Head.2 | X39901 | Sin_M_Head
Sin_M_Head.3 | X39902 | Sin_M_Head
Sin_M_Gonad.2 | X40767 | Sin_M_Gonad
Sin_M_Gonad.3 | X40768 | Sin_M_Gonad
Sin_F_Head.2 | X40769 | Sin_F_Head
Sin_F_Head.3 | X40770 | Sin_F_Head
Sin_F_Gonad.2 | X40771 | Sin_F_Gonad
Sin_F_Gonad.3 | X40772 | Sin_F_Gonad
Aib_F_Head.2 | X40773 | Aib_F_Head
Aib_F_Head.3 | X40774 | Aib_F_Head
Aib_F_Gonad.2 | X40775 | Aib_F_Gonad
Aib_F_Gonad.3 | X40776 | Aib_F_Gonad
Kaz_M_Head | X41803 | Kaz_M_Head
Kaz_M_Head.1 | X41804 | Kaz_M_Head
Kaz_M_Gonad | X41805 | Kaz_M_Gonad
Kaz_M_Gonad.1 | X41806 | Kaz_M_Gonad
Kaz_M_Tho | X41807 | Kaz_M_Tho
Kaz_M_Tho.1 | X41808 | Kaz_M_Tho
Kaz_F_Head | X41809 | Kaz_F_Head
Kaz_F_Head.1 | X41810 | Kaz_F_Head
Kaz_F_Tho | X41826 | Kaz_F_Tho
Kaz_F_Tho.1 | X41827 | Kaz_F_Tho
Kaz_F_Gonad | X41828 | Kaz_F_Gonad
Kaz_F_Gonad.1 | X41829 | Kaz_F_Gonad
Sin_F_Tho | X45052 | Sin_F_Tho
Sin_F_Tho.1 | X45053 | Sin_F_Tho
Aib_F_Tho.2 | X45054 | Aib_F_Tho
Aib_F_Tho.3 | X45055 | Aib_F_Tho
UrmAs_F_Head | X45056 | UrmAs_F_Head
UrmAs_F_Head.1 | X45057 | UrmAs_F_Head
UrmAs_F_Tho | X45058 | UrmAs_F_Tho
UrmAs_F_Tho.1 | X45059 | UrmAs_F_Tho
UrmAs_F_Gonad | X45060 | UrmAs_F_Gonad
UrmAs_F_Gonad.1 | X45061 | UrmAs_F_Gonad
Ata_F_Head | X45191 | Ata_F_Head
Ata_F_Head.1 | X45192 | Ata_F_Head
Ata_F_Tho | X45193 | Ata_F_Tho
Ata_F_Tho.1 | X45194 | Ata_F_Tho
Ata_F_Gonad | X45195 | Ata_F_Gonad
Ata_F_Gonad.1 | X45196 | Ata_F_Gonad
UrmSex_M_Head | X61773 | UrmSex_M_Head
UrmSex_M_Gonad | X61774 | UrmSex_M_Gonad
UrmSex_F_Head | X61775 | UrmSex_F_Head
UrmSex_F_Gonad | X61776 | UrmSex_F_Gonad
UrmSex_M_Head.1 | X61777 | UrmSex_M_Head
UrmSex_M_Gonad.1 | X61778 | UrmSex_M_Gonad
UrmSex_F_Head.1 | X61779 | UrmSex_F_Head
UrmSex_F_Gonad.1 | X61780 | UrmSex_F_Gonad

</details>

## 5. Load data, format, and get FPKM values

Pick a few parameters:

```
#R version 3.6.1 (2019-07-05)

##########Pick a few parameters
###Working directory
setwd("/Users/bvicoso/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1")
###tissue to use (Head, Gonad or Tho)
tissue<-"Gonad"

###Samples to exclude
bad<-c("Aib_F_Gonad.3", "Aib_F_Gonad.2", "Aib_F_Head.3", "Aib_F_Head.2", "Aib_F_Tho.2", "Aib_F_Tho.3", "Kaz_M_Head")
#what fraction of sexual individuals to use in training (versus test) set
fractionp<-0.8
#minimum average expression (if using)
minav<-5
```

Load and format data:

```
#######load data and sample information
exp<-read.table("females_asex_vs_sex_results_new_filteredGreater500bp.txt", sep="\t", head=T)
info<-read.table("SampleInfo.txt", sep=" ", head=F)
colnames(info)<-c("code", "Sample", "Type")
exp2<-exp[grep("count", colnames(exp))]
rownames(exp2) <- exp[,1]
colClean <- function(x){ colnames(x) <- gsub("\\.count", "", colnames(x)); x } 
exp2 <- colClean(exp2)
#transpose
exp3<-t(exp2)
head(info,2)
#add sample information
full<-merge(info, exp3, by.x="code", by.y="row.names")[,-1]
row.names(full)<-make.names(full[,1], unique=TRUE)
full<-full[,-1]
head(full[,1:5])
```

Get RPKM table:

Let's make an FPKM table for female samples, after excluding bad samples. We have the data loaded so we can start at ##Separate tissue. Make sure to filter out the right samples:

```
####Possible filters!!! Check that they are correct!!

#####sort so samples are in alphabetical order
full<-full[ order(row.names(full)), ]

#Remove outlier samples that we stored in vector "bad"
final2<-(subset(full, !(rownames(full) %in% bad)))
#(or use only specific list of samples)
#names<-c("Kaz_F_Gonad", "UrmAs_F_Gonad.1", "UrmSex_F_Gonad", "UrmAs_F_Gonad", "Kaz_F_Gonad.1")
#final2<-subset(final2, rownames(final2) %in% names)

##########Separate tissue
final1<-as.data.frame(final2[grep(tissue, rownames(final2)),][,1])
colnames(final1)<-c("Type")
final2<-final2[grep(tissue, rownames(final2)),][,2:ncol(final2)]

```

Create the sampletable and the DESeq2 object, and extract fpkm values:

```
#####sort so samples are in alphabetical order
final2<-final2[ order(row.names(final2)), ]
#transpose to right format
count_matrix<-t(final2)


######Make table with sample information
samples<-as.data.frame(colnames(count_matrix))
rownames(samples)<-colnames(count_matrix)
sex<-as.data.frame(samples[grep("Kaz|UrmSex|Sin", rownames(samples)),])
colnames(sex)<-"Samplename"
sex$Status<-"S"
asex<-as.data.frame(samples[grep("Aib|Ata|UrmAs", rownames(samples)),])
colnames(asex)<-"Samplename"
asex$Status<-"A"
sampletable<-rbind(sex, asex)
rownames(sampletable)<-sampletable$Samplename
sampletable<-sampletable[ order(row.names(sampletable)), ]

########create the DESeq object
library("DESeq2")
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix, colData = sampletable, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)

#Get FPKM
mcols(dds)$basepairs <-exp$len # the raw counts
fpkm<-as.data.frame(fpkm(dds))
head(fpkm)
```

## 5. Discriminant analysis with MASS

Normalize the data:

```
#keep only genes with mean TPM accross samples of >(minav), and apply log2
#final3<-fpkm[rowSums(fpkm)>minav*ncol(fpkm),]
#filter out genes with any sample <1
final3<-(fpkm[rowSums(fpkm > 1) == ncol(fpkm), ])
final3<-log2(final3)

###Normalize
bolFMat<-as.matrix(final3, nrow(final3), ncol(final3))
library(NormalyzerDE)
x<-performCyclicLoessNormalization(bolFMat, noLogTransform = T)
newDataFrame <- as.data.frame(x)
colnames(newDataFrame)<-colnames(final3)
rownames(newDataFrame)<-rownames(final3)
final4<-newDataFrame
###Check that normalization worked!
par(mar=c(10,1,1,1))
boxplot(final4, las=2)
par(mar=c(4,4,4,4))

```

Get principal components and format final files:

```
#re-transpose and add row names and types
final4<-t(final4)
rownames(final1)<-rownames(final4)

########Get principal components####
#if using PCR:
# prfinal<-prcomp(final4, scale=TRUE) ## scaling was failing because some columns had 0 variance, we remove them in line below
prfinal<-prcomp(final4[ , which(apply(final4, 2, var) != 0)], scale=TRUE)
final5<-prfinal$x


final<-merge(final1, final5, by.x="row.names", by.y="row.names")
rownames(final)<-final[,1]
final<-final[,-1]


#separate sexual and asexual species
finalsex<-final[grep("Sin|Kaz|UrmSex", rownames(final)),]
finalasex<-final[grep("Aib|UrmAs|Ata", rownames(final)),]

```

Run the discriminant analysis with MASS (and caret for partitioning the data, although it is not clear that we have to do this).

```

#####START LDA analysis

finalsex<-data.frame(finalsex)
# Load the caret library
library(caret)
# Split the data into training and test set 
set.seed(123)
training.samples <- createDataPartition(finalsex$Type, p = fractionp, list = FALSE)
train.data <- finalsex[training.samples, ]
test.data <- finalsex[-training.samples, ]

####OPTIONAL: Automate the choice of number of PCAs to use
library(MASS)
 # number of PCs to plot
testpc<-(c(0,0))
 i <- 2
 while (i < (ncol(train.data)-1 )) {
 print(i)
 model <- lda(Type~., data = train.data[,1:(i+1)])
# Add predictions on all sexuals just to get their LD1
predictions3 <- predict(model, finalsex)
# Model accuracy
temptestpc<-(c(i, mean(predictions3$class== finalsex$Type)))
testpc<-(rbind(testpc, temptestpc))
i = i+1
 }
#good PC is the smallest one for which a full separation of males and females is obtained
npca<-testpc[order(-testpc[,2])][1]
#####END: Automate the choice of number of PCs to use

#### Run standard analysis
library(MASS)
# Fit the model
#model <- lda(Type~., data = train.data)
##WITH PCs, USE ONLY THE MAIN ONES!
model <- lda(Type~., data = train.data[,1:(npca+1)])

# Make predictions
predictions <- predict(model, test.data)
# Model accuracy
mean(predictions$class== test.data$Type)

#### Make predictions for asexuals
predictions2 <- predict(model, finalasex)
# Model accuracy
mean(predictions2$class== finalasex$Type)

# Add predictions on all sexuals just to get their LD1
predictions3 <- predict(model, finalsex)
# Model accuracy
mean(predictions3$class== finalsex$Type)

#Create final LD1 datasets for females, asexuals and males
LD<-predictions3$x
femaleLD<-as.data.frame(LD[grep("F", rownames(LD)),])
maleLD<-as.data.frame(LD[grep("M", rownames(LD)),])
colnames(femaleLD)<-c("LD1")
colnames(maleLD)<-c("LD1")
asexLD<-as.data.frame(predictions2$x)

write.table(femaleLD, paste("LDA", tissue, npca, fractionp, ".txt", sep="_"), append = FALSE, quote = F, sep = " ")
write.table(maleLD, paste("LDA", tissue, npca, fractionp, ".txt", sep="_"), append = T, quote = F, sep = " ")
write.table(asexLD, paste("LDA", tissue, npca, fractionp, ".txt", sep="_"), append = T, quote = F, sep = " ")


```

Now plot the LD1 for females, males and asexuals.

```
library("plotrix")
colset<-smoothColors("white",39,"indianred")
colf<-colset[35]
cola<-colset[20]
colm<-"lightblue4"

# Get limits of the plot and assign x-coordinates to males, females and asexuals
maxval<-max(c(femaleLD$LD1, asexLD$LD1, maleLD$LD1))+0.2
minval<-min(c(femaleLD$LD1, asexLD$LD1, maleLD$LD1))-0.2
femaleLD$coord<-1
asexLD$coord<-2
maleLD$coord<-3

#start plot
pdf(paste("LDA", tissue, npca, fractionp, ".pdf", sep="_"), width=3.5, height=3.5)
plot(femaleLD$coord, femaleLD$LD1, xlim=c(0,4), ylim=c(minval, maxval), ylab="LD1", xlab="", col="grey", pch=20, axes=FALSE, main=tissue)
Axis(side=1, at=c(-1,1,2,3), labels=c("", "F", "Asex", "M"), tick = F)
segments(-1,minval, 20, minval, lwd=1)
segments(-0.158,minval, -0.158, maxval, lwd=1)
Axis(side=2, labels=T)
points(1, median(femaleLD$LD1, na.rm=T), col=colf, pch=19)
 segments(1,quantile(femaleLD$LD1, 0.25), 1, quantile(femaleLD$LD1, 0.75), col= colf)

points(asexLD$coord, asexLD$LD1, col="grey", pch=20)
points(2, median(asexLD$LD1, na.rm=T), col=cola, pch=19)
 segments(2,quantile(asexLD$LD1, 0.25), 2, quantile(asexLD$LD1, 0.75), col= cola)
points(maleLD$coord, maleLD$LD1, col="grey", pch=20)
points(3, median(maleLD$LD1, na.rm=T), col=colm, pch=19)
 segments(3,quantile(maleLD$LD1, 0.25), 3, quantile(maleLD$LD1, 0.75), col= colm)
dev.off()
 ```

<details>

<summary>Plot to add to figure 3 (Gonad)</summary>

```
#set colors for male, female, asexual
dev.new(width=5, height=4)
par(mar=c(2,4,1,1))
library("plotrix")
colset<-smoothColors("white",39,"indianred")
colf<-colset[35]
cola<-colset[20]
colm<-"lightblue4"
plot(femaleLD$coord, femaleLD$LD1, xlim=c(0.5,3.5), ylim=c(minval, round(maxval, digits=0)+1), ylab="", xlab="", col="grey", pch=19, axes=FALSE, main="")
mtext(side = 2, text = "LD1", line = 2)
Axis(side=1, at=c(-1,1,2,3), labels=c("", "Female", "Asexual", "Male"), tick = F, padj = -1)
box(which="plot")

Axis(side=2, labels=T)
points(1, median(femaleLD$LD1, na.rm=T), col=colf, pch=19)
draw.circle(1, median(femaleLD$LD1, na.rm=T), 0.06,nv=100, border=colf ,col= colf,lty=1,density=NULL, angle=45,lwd=1) 
segments(1,quantile(femaleLD$LD1, 0.25), 1, quantile(femaleLD$LD1, 0.75), col= colf, lwd=2)

points(asexLD$coord, asexLD$LD1, col="grey", pch=19)
points(2, median(asexLD$LD1, na.rm=T), col=cola, pch=19)
draw.circle(2, median(asexLD$LD1, na.rm=T), 0.06,nv=100, border=cola ,col= cola,lty=1,density=NULL, angle=45,lwd=1) 
 segments(2,quantile(asexLD$LD1, 0.25), 2, quantile(asexLD$LD1, 0.75), col= cola, lwd=2)
points(maleLD$coord, maleLD$LD1, col="grey", pch=19)
points(3, median(maleLD$LD1, na.rm=T), col=colm, pch=19)
draw.circle(3, median(maleLD$LD1, na.rm=T), 0.06,nv=100, border=colm ,col= colm,lty=1,density=NULL, angle=45,lwd=1) 
 segments(3,quantile(maleLD$LD1, 0.25), 3, quantile(maleLD$LD1, 0.75), col= colm, lwd=2)
#Add significance
wilcox.test(asexLD$LD1, femaleLD$LD1)
wilcox.test(asexLD$LD1, maleLD$LD1)

segments(1,max(femaleLD$LD1)+0.2, 1, max(femaleLD$LD1)+0.6, col= "black", lwd=1)
segments(2,max(femaleLD$LD1)+0.2, 2, max(femaleLD$LD1)+0.6, col= "black", lwd=1)
segments(1,max(femaleLD$LD1)+0.6, 2, max(femaleLD$LD1)+0.6, col= "black", lwd=1)

segments(2,max(maleLD$LD1)+0.2, 2, max(maleLD$LD1)+0.6, col= "black", lwd=1)
segments(3,max(maleLD$LD1)+0.2, 3, max(maleLD$LD1)+0.6, col= "black", lwd=1)
segments(2,max(maleLD$LD1)+0.6, 3, max(maleLD$LD1)+0.6, col= "black", lwd=1)

text(1.5, max(femaleLD$LD1)+1.2, "NS.", cex=1.9)
text(2.5, max(maleLD$LD1)+1, "***", cex=2)
```

</details>

<details>
<summary>Plot to add to Head supplementary figure</summary>

```
#set colors for male, female, asexual
dev.new(width=5, height=4)
par(mar=c(2,4,1,1))
library("plotrix")
colset<-smoothColors("white",39,"indianred")
colf<-colset[35]
cola<-colset[20]
colm<-"lightblue4"
plot(femaleLD$coord, femaleLD$LD1, xlim=c(0.5,3.5), ylim=c(minval, round(maxval, digits=0)+1), ylab="", xlab="", col="grey", pch=19, axes=FALSE, main="")
mtext(side = 2, text = "LD1", line = 2)
Axis(side=1, at=c(-1,1,2,3), labels=c("", "Female", "Asexual", "Male"), tick = F, padj = -1)
box(which="plot")

Axis(side=2, labels=T)
points(1, median(femaleLD$LD1, na.rm=T), col=colf, pch=19)
draw.circle(1, median(femaleLD$LD1, na.rm=T), 0.06,nv=100, border=colf ,col= colf,lty=1,density=NULL, angle=45,lwd=1) 
segments(1,quantile(femaleLD$LD1, 0.25), 1, quantile(femaleLD$LD1, 0.75), col= colf, lwd=2)

points(asexLD$coord, asexLD$LD1, col="grey", pch=19)
points(2, median(asexLD$LD1, na.rm=T), col=cola, pch=19)
draw.circle(2, median(asexLD$LD1, na.rm=T), 0.06,nv=100, border=cola ,col= cola,lty=1,density=NULL, angle=45,lwd=1) 
 segments(2,quantile(asexLD$LD1, 0.25), 2, quantile(asexLD$LD1, 0.75), col= cola, lwd=2)
points(maleLD$coord, maleLD$LD1, col="grey", pch=19)
points(3, median(maleLD$LD1, na.rm=T), col=colm, pch=19)
draw.circle(3, median(maleLD$LD1, na.rm=T), 0.06,nv=100, border=colm ,col= colm,lty=1,density=NULL, angle=45,lwd=1) 
 segments(3,quantile(maleLD$LD1, 0.25), 3, quantile(maleLD$LD1, 0.75), col= colm, lwd=2)
#Add significance
wilcox.test(asexLD$LD1, femaleLD$LD1)
wilcox.test(asexLD$LD1, maleLD$LD1)

segments(1,max(asexLD$LD1)+0.2, 1, max(asexLD$LD1)+0.6, col= "black", lwd=1)
segments(2,max(asexLD$LD1)+0.2, 2, max(asexLD$LD1)+0.6, col= "black", lwd=1)
segments(1,max(asexLD$LD1)+0.6, 2, max(asexLD$LD1)+0.6, col= "black", lwd=1)

segments(2,max(maleLD$LD1)+0.2, 2, max(maleLD$LD1)+0.6, col= "black", lwd=1)
segments(3,max(maleLD$LD1)+0.2, 3, max(maleLD$LD1)+0.6, col= "black", lwd=1)
segments(2,max(maleLD$LD1)+0.6, 3, max(maleLD$LD1)+0.6, col= "black", lwd=1)

text(1.5, max(asexLD$LD1)+1.3, "NS.", cex=1.9)
text(2.5, max(maleLD$LD1)+1, "**", cex=2)
```

</details>

<details>

<summary>Plot to add to Thorax supplementary figure</summary>

```
#set colors for male, female, asexual
dev.new(width=5, height=4)
par(mar=c(2,4,1,1))
library("plotrix")
colset<-smoothColors("white",39,"indianred")
colf<-colset[35]
cola<-colset[20]
colm<-"lightblue4"
plot(femaleLD$coord, femaleLD$LD1, xlim=c(0.5,3.5), ylim=c(minval, round(maxval, digits=0)+3), ylab="", xlab="", col="grey", pch=19, axes=FALSE, main="")
mtext(side = 2, text = "LD1", line = 2)
Axis(side=1, at=c(-1,1,2,3), labels=c("", "Female", "Asexual", "Male"), tick = F, padj = -1)
box(which="plot")

Axis(side=2, labels=T)
points(1, median(femaleLD$LD1, na.rm=T), col=colf, pch=19)
draw.circle(1, median(femaleLD$LD1, na.rm=T), 0.06,nv=100, border=colf ,col= colf,lty=1,density=NULL, angle=45,lwd=1) 
segments(1,quantile(femaleLD$LD1, 0.25), 1, quantile(femaleLD$LD1, 0.75), col= colf, lwd=2)

points(asexLD$coord, asexLD$LD1, col="grey", pch=19)
points(2, median(asexLD$LD1, na.rm=T), col=cola, pch=19)
draw.circle(2, median(asexLD$LD1, na.rm=T), 0.06,nv=100, border=cola ,col= cola,lty=1,density=NULL, angle=45,lwd=1) 
 segments(2,quantile(asexLD$LD1, 0.25), 2, quantile(asexLD$LD1, 0.75), col= cola, lwd=2)
points(maleLD$coord, maleLD$LD1, col="grey", pch=19)
points(3, median(maleLD$LD1, na.rm=T), col=colm, pch=19)
draw.circle(3, median(maleLD$LD1, na.rm=T), 0.06,nv=100, border=colm ,col= colm,lty=1,density=NULL, angle=45,lwd=1) 
 segments(3,quantile(maleLD$LD1, 0.25), 3, quantile(maleLD$LD1, 0.75), col= colm, lwd=2)
#Add significance
wilcox.test(asexLD$LD1, femaleLD$LD1)
wilcox.test(asexLD$LD1, maleLD$LD1)

segments(1,max(asexLD$LD1)+0.2, 1, max(asexLD$LD1)+0.6, col= "black", lwd=1)
segments(2,max(asexLD$LD1)+0.2, 2, max(asexLD$LD1)+0.6, col= "black", lwd=1)
segments(1,max(asexLD$LD1)+0.6, 2, max(asexLD$LD1)+0.6, col= "black", lwd=1)

segments(2,max(asexLD$LD1)+1.4, 2, max(asexLD$LD1)+1.8, col= "black", lwd=1)
segments(3,max(asexLD$LD1)+1.4, 3, max(asexLD$LD1)+1.8, col= "black", lwd=1)
segments(2,max(asexLD$LD1)+1.8, 3, max(asexLD$LD1)+1.8, col= "black", lwd=1)

text(1.5, max(asexLD$LD1)+1.7, "NS.", cex=1.9)
text(2.5, max(asexLD$LD1)+2.9, "NS", cex=1.9)
```

</details>
