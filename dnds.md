# dN and dS estimation

We used the codon-aware TranslatorX to align coding sequences that are reciprocal best hits between A. sinica and each of the other species (using Blat). KaKs_calculator was used to estimate dN and dS. The wrapper [dNdS.pl](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/scripts/dNdS.pl) was used for this purpose:

```
#load any module you need here
module load blat/20170224
module load gblocks/0.91b
module load muscle

#run perl script
perl dS_polyp2dip.pl carp.fa.cds barb.fa.cds output/ 
```
