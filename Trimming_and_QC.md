# Read Trimming and Quality Control for RNAseq Libraries

## General Computing Resource Requirements

We use slurm for running jobs. Please adjust to your needs (i.e. larger size and number of libraries may require more memory and/or longer run time etc.).

```
#!/bin/bash
#
#$ -S /bin/bash
#$ -v TST=abc
#$ -M ahuylman@ist.ac.at
#$ -m ea
#$ -l mf=200G
#$ -l h_vmem=30G
#$ -l h_rt=60:00:00
#$ -cwd
```

## Running Trimmomatic and FastQC
Reads are trimmed with Trimmomatic (version 0.36). TruSeq3 adapters, the first 14 bases (HEADCROP:14), leading low quality or N bases (below quality 3) (LEADING:3), and trailing low quality or N bases (below quality 3) (TRAILING:3) are removed. Only reads with a minimum read length of 90 base pairs were retained.

```
SAMPLES = (41805,41806,41807,41808,41809,41810,41826,41827,41828,41829)

SPECIES="ArtemiaKazakhstan"

for k in ${SAMPLES}; do echo $k; java -Xmx1024m -jar ~/Trimmomatic-0.36/trimmomatic-0.36.jar PE -phred33 ~/Artemia/RNAseq/"$SPECIES"/untrimmed/$k*.1.fastq.gz ~/Artemia/RNAseq/"$SPECIES"/untrimmed/$k*.2.fastq.gz ~/Artemia/RNAseq/"$SPECIES"/trimmed/"$k"_1_trimmed_paired.fq.gz ~/Artemia/RNAseq/"$SPECIES"/trimmed/"$k"_1_trimmed_unpaired.fq.gz ~/Artemia/RNAseq/"$SPECIES"/trimmed/"$k"_2_trimmed_paired.fq.gz ~/Artemia/RNAseq/"$SPECIES"/trimmed/"$k"_2_trimmed_unpaired.fq.gz ILLUMINACLIP:~/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 HEADCROP:14 LEADING:3 TRAILING:3 MINLEN:90; ~/FastQC/fastqc ~/Artemia/RNAseq/"$SPECIES"/trimmed/"$k"*trimmed*.fq.gz -o ~/Artemia/RNAseq/"$SPECIES"/QC/; done
```
