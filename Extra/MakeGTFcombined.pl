#!/usr/local/bin/perl
#this program goes through a fasta file of transcripts
#And ajusts the corresponding VCF file to match mapping to a combined fasta file
#usage: perl AdjustVCF.pl transcripts.fa MappedSample.vcf

$/ = ">";


my $input = $ARGV[0];
my $vcf = $ARGV[1];
open (INPUT, "$input") or die "can't find $input";
open (RESULTS, ">$vcf.fake");
$suml=0;


while ($line=<INPUT>)
	{
	#skip first (empty) line
	next if $. == 1;
	chomp $line;
	#get sequence length
	(@dnas)=split ("\n", $line);
	$definition=$dnas[0];
	shift @dnas;
	$sequence=join ("", @dnas);
	$cdslength=length($sequence);
	#store the adjustment factor for this transcript
	$adjust{$definition} = $suml;

	#Add length to adjustment factor for next gene
	$suml=$suml+$cdslength;
	}

print "test $adjust{$definition}\n";

$/ = "\n";

open (VCF, "$vcf") or die "can't find $vcf";
while ($snp=<VCF>)
	{
	if ($snp =~ /^#CHROM/)
		{
		print RESULTS $snp;
		}
	elsif ($snp =~ /^#/)
		{
		}
	else
		{
		chomp $snp;
		(@vcfcols)=split ("\t", $snp);
		$startadjust = $vcfcols[1]+ $adjust{$vcfcols[0]};
		print RESULTS "fasta1\t$startadjust\t$vcfcols[2]\t$vcfcols[3]\t$vcfcols[4]\t$vcfcols[5]\t$vcfcols[6]\t$vcfcols[7]\t$vcfcols[8]\t$vcfcols[9]\n";
		}
	}
