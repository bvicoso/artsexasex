# Transitions to asexuality and evolution of gene expression in Artemia brine shrimp

This Git page contains pipelines and important data files used in Huylmans et al (2021).

The raw sequencing data is available at  NCBI SRA under project number PRJNA748528; a compressed version of all data files listed here is available on the IST Austria data depository _[final URL to be added upon acceptance]_.

## Data files

* **transcriptomes for each species** are available [here](https://seafile.ista.ac.at/d/063dab9e21b14c6fad32/).
* **sample information table**: this table contains the correspondence between the sample numbers (see counts table below) and their species/tissue/sex. It is available [here](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/data/SampleInfo.txt).
* **table of read counts** : [link](https://seafile.ista.ac.at/f/181d9e97950543ec87a6/?dl=1). This file also contains the differential expression p-values between sexual and asexuals, and males and females in A. sinica, for each gene.
* The **VCF** containing the SNP calls using for the Fst estimates is [here](https://seafile.ista.ac.at/f/353654a226954d13bee1/?dl=1). 

## Pipelines

* [Reads trimming and quality control](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/Trimming_and_QC.md)
* [Transcriptome assemblies](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/TranscriptomeAssemblies.txt) 
* [SNP calling and Fst estimation](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/Fst.md)
* [Read mapping and counting](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/Mapping_and_Counting.md)
* [DESeq2 for differentially expressed genes between sexual and asexual females](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/asexGenes.md)
* [DESeq2 analysis for sex-biased genes, and plotting of gene expression change in different lineages](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/SB_SexAsex.md)
* [PCA-LDA to estimate "maleness score" of each sample](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/PCA_LDA.md)
* [dN and dS estimation](https://git.ista.ac.at/bvicoso/artsexasex/-/blob/master/dnds.md)
