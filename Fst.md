# SNP calling and Fst calculations

## SNP calling

### Mapping

```
#Index A. sinica transcriptome (only transcripts > 500bps used)
bwa index Asinica500.fa

# For each sample:
bwa mem -M -t 30 Asinica500.fa read1.fastq.gz read2.fastq.gz | samtools view -b | samtools sort -T individual > individual_sort.bam
```

### SNP calling

```
#for all files
#create VCF
srun bcftools mpileup -a AD,DP,SP -Ou -f Asinica500.fa aib_40773.bam aib_40774.bam Aib_101424.bam Aib_101425.bam Aib_101426.bam Aib_101427.bam Aib_101428.bam Aib_101429.bam Aib_101440.bam Aib_101441.bam Aib_40775.bam Aib_40776.bam Aib_45054.bam Aib_45055.bam ata_45191.bam ata_45192.bam Ata_45193.bam Ata_45194.bam Ata_45195.bam Ata_45196.bam kaz_41809.bam kaz_41810.bam Kaz_39873.bam Kaz_39874.bam Kaz_39875.bam Kaz_39876.bam Kaz_41803.bam Kaz_41804.bam Kaz_41805.bam Kaz_41806.bam Kaz_41807.bam Kaz_41808.bam Kaz_41826.bam Kaz_41827.bam Kaz_41828.bam Kaz_41829.bam sin_39897.bam sin_39898.bam Sin_39869.bam Sin_39870.bam Sin_39871.bam Sin_39872.bam Sin_39877.bam Sin_39878.bam Sin_39879.bam Sin_39880.bam Sin_39895.bam Sin_39896.bam Sin_39899.bam Sin_39900.bam Sin_39901.bam Sin_39902.bam Sin_40767.bam Sin_40768.bam Sin_40769.bam Sin_40770.bam Sin_40771.bam Sin_40772.bam Sin_45052.bam Sin_45053.bam urmas_45056.bam urmas_45057.bam UrmAs_45058.bam UrmAs_45059.bam UrmAs_45060.bam UrmAs_45061.bam urmsex_61775.bam urmsex_61779.bam UrmSex_61773.bam UrmSex_61774.bam UrmSex_61776.bam UrmSex_61777.bam UrmSex_61778.bam UrmSex_61780.bam | bcftools call -v -f GQ,GP -mO z -o AllSamples.vcf.gz

#filter
srun vcftools --gzvcf AllSamples.vcf.gz  --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100 --recode --stdout >  AllSamples_filtered.vcf

# remove multiallelic
srun bcftools view --max-alleles 2 --exclude-types indels AllSamples_filtered.vcf > AllSamples_filtered2.vcf
```

## Fst calculation

### VCF format and sample selection:

The VCF has the following columns:
```
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	aib_40773.bam ...
```

And the format of the output for each individual is: GT:PL:DP:SP:AD:GP:GQ
* GT = genotype
* PL = List of Phred-scaled genotype likelihoods
* DP = Number of high-quality bases
* SP = Phred-scaled strand bias P-value
* AD = Allelic depths (two numbers: #ref,#alternative)
* GP, GQ = Phred-scaled Genotype posterior probabilities & Quality

What we care about is AD, the number of reads that support the reference and alternative allele. 

Our program will:
- pick the columns that correspond to each population
- sum, for each population, how many reads support the reference and the alternative allele
- filter out SNPs for which the total minor allele count is below a threshold
- estimate p1, q1 = 1-p1, p2, q2=1-p2
- calculate Nei's Fst using the formula provided in Will's paper:
[(p1 - p2)^2]/[(p1+p2)(q1+q2)]

To make life easier, we can get a column number for each sample, since we will need to provide it to the script:

```
grep -v "^##"  AllSamples_filtered2.vcf | head -1 | perl -pi -e 's/\t/\n/gi' | grep -n "" 
```

Let's start with head files with new aibi lake:
* sin_39897.bam sin_39898.bam:  46,47
* urmsex_61775.bam urmsex_61779.bam: 76,77
* kaz_41809.bam kaz_41810.bam: 30,31
* Aib_101424.bam Aib_101425.bam: 12,13
* ata_45191.bam ata_45192.bam: 24,25
* urmas_45056.bam urmas_45057.bam: 70,71



## Fst calculation with minor frequency allele filter

We use a minimum total allele frequency of 0.1 to exclude very low frequency SNPs.

###  Script

The script is [here](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/scripts/Fst_calculator_MAF.pl).

The output file has the following columns:
* chromosome/scaffold
* coordinate
* Number of reads supporting the reference allele in population 1
* Number of reads supporting the alternative allele in population 1 
* p1, the frequency of the reference allele in population 1 
* q1, the frequency of the alternative allele in population 1 
* Number of reads supporting the reference allele in population 2
* Number of reads supporting the alternative allele in population 2 
* p1, the frequency of the reference allele in population 2 
* q1, the frequency of the alternative allele in population 2 
* Nei's fst

### Run script

```
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47 12,13 Asin_Aaib_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47 76,77 Asin_Aurmsex_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47 30,31 Asin_Akaz_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47 24,25 Asin_Aata_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47 70,71 Asin_Aumas_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77 30,31 Aurmsex_Akaz_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77 12,13 Aurmsex_Aaib_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77 24,25 Aurmsex_Aata_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77 70,71 Aurmsex_Aurmas_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 30,31 12,13 Akaz_Aaib_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 30,31 24,25 Akaz_Aata_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 30,31 70,71 Akaz_Aurmas_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 12,13 24,25 Aaib_Aata_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 12,13 70,71 Aaib_Aurmas_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 24,25 70,71 Aata_Aurmas_head

```

### Re-calculate Fst excluding admixed/contaminated A. atanasovsko (sample ata_45191 = column 24 in VCF)

```
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47 25 Asin_AataNo91_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77 25 Aurmsex_AataNo91_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 30,31 25 Akaz_AataNo91_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 12,13 25 Aaib_AataNo91_head
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 25 70,71 Aata_AurmasNo91_head
```

## Plot in Rcode

### Load data and check median Fst for each


Load all data:

<details>
  <summary>Click for R code for the loading data including all samples (as in Fig. 1)!</summary>

```
f1<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aurmsex_head.maf.fst", head=F, sep=" ")
f2<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Akaz_head.maf.fst", head=F, sep=" ")
f3<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aaib_head.maf.fst", head=F, sep=" ")
f4<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aata_head.maf.fst", head=F, sep=" ")
f5<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aumas_head.maf.fst", head=F, sep=" ")

f6<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Akaz_head.maf.fst", head=F, sep=" ")
f7<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aaib_head.maf.fst", head=F, sep=" ")
f8<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aata_head.maf.fst", head=F, sep=" ")
f9<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aurmas_head.maf.fst", head=F, sep=" ")

f10<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aaib_head.maf.fst", head=F, sep=" ")
f11<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aata_head.maf.fst", head=F, sep=" ")
f12<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aurmas_head.maf.fst", head=F, sep=" ")

f13<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aaib_Aata_head.maf.fst", head=F, sep=" ")
f14<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aaib_Aurmas_head.maf.fst", head=F, sep=" ")
f15<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aata_Aurmas_head.maf.fst", head=F, sep=" ")
```
</details>

Or without the strange A. atanasovsko sample:

<details>
  <summary>Click for R code for the loading Fst values calculated without sample #91!</summary>

```
f1<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aurmsex_head.maf.fst", head=F, sep=" ")
f2<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Akaz_head.maf.fst", head=F, sep=" ")
f3<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aaib_head.maf.fst", head=F, sep=" ")
f4<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_AataNo91_head.maf.fst", head=F, sep=" ")
f5<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aumas_head.maf.fst", head=F, sep=" ")

f6<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Akaz_head.maf.fst", head=F, sep=" ")
f7<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aaib_head.maf.fst", head=F, sep=" ")
f8<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_AataNo91_head.maf.fst", head=F, sep=" ")
f9<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aurmas_head.maf.fst", head=F, sep=" ")

f10<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aaib_head.maf.fst", head=F, sep=" ")
f11<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_AataNo91_head.maf.fst", head=F, sep=" ")
f12<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aurmas_head.maf.fst", head=F, sep=" ")

f13<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aaib_AataNo91_head.maf.fst", head=F, sep=" ")
f14<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aaib_Aurmas_head.maf.fst", head=F, sep=" ")
f15<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aata_AurmasNo91_head.maf.fst", head=F, sep=" ")
```
</details>

Check median and mean of each:

<details>
  <summary>Click for R code for mean and median!</summary>

```
sinF<-c(mean(f1$V11), mean(f2$V11), mean(f3$V11), mean(f4$V11), mean(f5$V11))
urmSexF<-c(mean(f6$V11), mean(f6$V11), mean(f8$V11), mean(f9$V11))
ataF<-c(mean(f11$V11), mean(f13$V11), mean(f15$V11))
kazF<-c(mean(f10$V11), mean(f12$V11))
asexF<-c(mean(f14$V11))
sinF
urmSexF
ataF
kazF
asexF

sinF<-c(median(f1$V11), median(f2$V11), median(f3$V11), median(f4$V11), median(f5$V11))
urmSexF<-c(median(f6$V11), median(f6$V11), median(f8$V11), median(f9$V11))
ataF<-c(median(f11$V11), median(f13$V11), median(f15$V11))
kazF<-c(median(f10$V11), median(f12$V11))
asexF<-c(median(f14$V11))
sinF
urmSexF
ataF
kazF
asexF
```

</details>

### Plot pairwise mean Fst


<details>
  <summary>Click for R code for pretty plot!</summary>

```
#####Plot to add to tree
dev.new(width=4.5, height=4)
library("plotrix")

par(mfrow=c(1,1))
par(mar=c(0,0,0,0))
#plot(0:10,type="n",axes=FALSE)
plot(0,type='n',axes=FALSE,ann=FALSE, xlim=c(1,7), ylim=c(1,6))

#creates gradient between two colors
smoothColors("red",39,"blue")

#plots gradient in rectangle
# get an empty box

 # run across the three primaries
 # now just a smooth gradient across the bar
 gradient.rect(6.5,1,7,6,col=smoothColors("white",48,"indianred"),border="black",gradient="y")

colset<-smoothColors("white",39,"indianred")
colf1<-colset[round(mean(f1$V11),digits=2)*100]
colf2<-colset[round(mean(f2$V11),digits=2)*100]
colf3<-colset[round(mean(f3$V11),digits=2)*100]
colf4<-colset[round(mean(f4$V11),digits=2)*100]
colf5<-colset[round(mean(f5$V11),digits=2)*100]
colf6<-colset[round(mean(f6$V11),digits=2)*100]
colf7<-colset[round(mean(f7$V11),digits=2)*100]
colf8<-colset[round(mean(f8$V11),digits=2)*100]
colf9<-colset[round(mean(f9$V11),digits=2)*100]
colf10<-colset[round(mean(f10$V11),digits=2)*100]
colf11<-colset[round(mean(f11$V11),digits=2)*100]
colf12<-colset[round(mean(f12$V11),digits=2)*100]
colf13<-colset[round(mean(f13$V11),digits=2)*100]
colf14<-colset[round(mean(f14$V11),digits=2)*100]
colf15<-colset[round(mean(f15$V11),digits=2)*100]

###rectangles

#Sinica
 sinrecx1<-c(1,2,3,4,5)
sinrecx2<-c(2,3,4,5,6)
sinrecy1<-c(5,5,5,5,5)
sinrecy2<-c(6,6,6,6,6)
sincol<-c(colf1, colf2, colf3, colf4, colf5)
rect(sinrecx1, sinrecy1, sinrecx2, sinrecy2, col= sincol)

#Urm_Sex
urmrecx1<-c(1,2,3,4)
urmrecx2<-c(2,3,4,5)
urmrecy1<-c(4,4,4,4,4)
urmrecy2<-c(5,5,5,5,5)
urmcol<-c(colf6, colf7, colf8, colf9)
rect(urmrecx1, urmrecy1, urmrecx2, urmrecy2, col= urmcol)

#Ata
atarecx1<-c(1,2,3)
atarecx2<-c(2,3,4)
atarecy1<-c(3,3,3,3)
atarecy2<-c(4,4,4,4)
atacol<-c(colf11, colf13, colf15)
rect(atarecx1, atarecy1, atarecx2, atarecy2, col= atacol)

#Kaz
kazrecx1<-c(1,2)
kazrecx2<-c(2,3)
kazrecy1<-c(2,2)
kazrecy2<-c(3,3)
kazcol<-c(colf10, colf12)
rect(kazrecx1, kazrecy1, kazrecx2, kazrecy2, col= kazcol)

#Asex
asexrecx1<-c(1)
asexrecx2<-c(2)
asexrecy1<-c(1)
asexrecy2<-c(2)
asexcol<-c(colf14)
rect(asexrecx1, asexrecy1, asexrecx2, asexrecy2, col= asexcol)
```
</details>

## Run Fst calculation with combined head and gonad reads

We can combine heads and gonads of each species to increase the coverage. Since the same individuals were used for both head and gonad dissections, this does not increase our sample size (and does not change the patterns noticeably).

Samples to use:
* sin_39897.bam sin_39898.bam 39899 39900:  46,47,58,59
* urmsex_61775.bam urmsex_61779.bam 61776 61780: 76,77,80,83
* kaz_41809.bam kaz_41810.bam 41828 41829: 30,31,44,45
* Aib_101424.bam Aib_101425.bam 101428 101429 : 12,13,16,17
* ata_45191.bam ata_45192.bam 45195 45196: 24,25,28,29
* urmas_45056.bam urmas_45057.bam 45060 45061: 70,71,74,75

### Run script

```
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47,58,59 12,13,16,17 Asin_Aaib_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47,58,59 76,77,80,83 Asin_Aurmsex_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47,58,59 30,31,44,45 Asin_Akaz_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47,58,59 24,25,28,29 Asin_Aata_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 46,47,58,59 70,71,74,75 Asin_Aumas_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77,80,83 30,31,44,45 Aurmsex_Akaz_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77,80,83 12,13,16,17 Aurmsex_Aaib_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77,80,83 24,25,28,29 Aurmsex_Aata_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 76,77,80,83 70,71,74,75 Aurmsex_Aurmas_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 30,31,44,45 12,13,16,17 Akaz_Aaib_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 30,31,44,45 24,25,28,29 Akaz_Aata_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 30,31,44,45 70,71,74,75 Akaz_Aurmas_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 12,13,16,17 24,25,28,29 Aaib_Aata_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 12,13,16,17 70,71,74,75 Aaib_Aurmas_HG
perl Fst_calculator_MAF.pl AllSamples_filtered2.vcf 24,25,28,29 70,71,74,75 Aata_Aurmas_HG
```

### Load data

<details>
  <summary>Click for R code for the loading data including all samples!</summary>

```
f1<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aurmsex_HG.maf.fst", head=F, sep=" ")
f2<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Akaz_HG.maf.fst", head=F, sep=" ")
f3<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aaib_HG.maf.fst", head=F, sep=" ")
f4<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aata_HG.maf.fst", head=F, sep=" ")
f5<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Asin_Aumas_HG.maf.fst", head=F, sep=" ")

f6<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Akaz_HG.maf.fst", head=F, sep=" ")
f7<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aaib_HG.maf.fst", head=F, sep=" ")
f8<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aata_HG.maf.fst", head=F, sep=" ")
f9<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aurmsex_Aurmas_HG.maf.fst", head=F, sep=" ")

f10<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aaib_HG.maf.fst", head=F, sep=" ")
f11<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aata_HG.maf.fst", head=F, sep=" ")
f12<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Akaz_Aurmas_HG.maf.fst", head=F, sep=" ")

f13<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aaib_Aata_HG.maf.fst", head=F, sep=" ")
f14<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aaib_Aurmas_HG.maf.fst", head=F, sep=" ")
f15<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1/Fst/Aata_Aurmas_HG.maf.fst", head=F, sep=" ")
```
</details>

### Plot

Use the same code as in the previous section.

