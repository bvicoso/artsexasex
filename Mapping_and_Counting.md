# Mapping of Reads to Transcriptome(s) and Counting of Mapping Reads
This is a bash script to call the programmes NextGenMap for read mapping and custom scripts for counting the number of mapping reads to the transcripts.

## General Computing Resource Requirements
We use slurm for running jobs. Please adjust to your needs (i.e. larger size and number of libraries may require more memory and/or longer run time etc.).

```
#!/bin/bash
#
#$ -S /bin/bash
#$ -v TST=abc
#$ -M ahuylman@ist.ac.at
#$ -m ea
#$ -l mf=5G
#$ -l h_vmem=5G
#$ -l h_rt=190:00:00
#$ -l mathematica=0
#$ -cwd
```

# Paths to Files and Programmes
Please adjust according to your system.

```
#Path to NextGenMap
NGM=~/NextGenMap-0.5.0/bin/ngm-0.5.0/ngm

#Path to reference
ref=~/Artemia/TranscriptomeAssembly/ArtemiaSinica/EviGene/ArtemiaSinica_EviGene_assembly.okay.cds

#Path to trimmed reads
reads=~/Artemia/RNAseq/ArtemiaSinica/trimmed

#Path to mapping output directory
mapping=~/Artemia/Mapping/ArtemiaSinica

#Path to Gene Extract file
geneExtract=~/Artemia/TranscriptomeAssembly/ArtemiaSinica/ArtemiaSinica_GeneExtract.out

#Path to scripts
scripts=~/Artemia/Scripts
```

## Map Reads
Run NextGenMap (version 0.5.0) to map reads to the A. sinica transcriptome or the individual transcriptomes.

```
export k 
for k in {39869,39870,39871,39872,39877,39878,39879,39880,39895,39896,39897,39898,39899,39900,39901,39902,40767,40768,40769,40770,40771,40772,45052,45053}; do ${NGM} -1 ${reads}/${k}_1_trimmed_paired.fq.gz -2 ${reads}/${k}_2_trimmed_paired.fq.gz -r ${ref} -o ${mapping}/${k}.sam -p; done
```

## Count Number of Mapping Reads
Use custom scripts.

```
python ${scripts}/ArtemiaGeneExtract.py -i ${ref} -o ${geneExtract}

export k
for k in {39869,39870,39871,39872,39877,39878,39879,39880,39895,39896,39897,39898,39899,39900,39901,39902,40767,40768,40769,40770,40771,40772,45052,45053}; do python ${scripts}/tCounterU_NGM.py ${geneExtract} ${mapping}/${k}.sam ${mapping}/${k}.tcount; done
```
