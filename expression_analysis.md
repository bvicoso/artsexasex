```
#R version 3.6.1 (2019-07-05)

##########Pick a few parameters
###Working directory
setwd("/home/ahuylman/1_Projects/Artemia/Scripts/Revision/")
###tissue to use (Head, Gonad or Tho)
tissue<-"Head"
### Pick pair of species to analyse
#Species for calling sex-biased genes (Asin, Akaz or AurmSexual) and ancestral gene expression
ancestral<-"Asin"
#Now pick the focal species "asexual"
#Options: Asin, Akaz, AurmSexual, Apar, Aata, AurmAsex
focal<-"Akaz"
###Samples to exclude
bad<-c("Apar_F_Gonad.3", "Apar_F_Gonad.2", "Apar_F_Head.3", "Apar_F_Head.2", "Apar_F_Tho.2", "Apar_F_Tho.3")


#######load data and sample information
#exp<-read.table("females_asex_vs_sex_results_new_filteredGreater500bp.txt", sep="\t", head=T)

Asex2Asin <- read.table("/home/ahuylman/1_Projects/Artemia/DifferentialGeneExpression/Asex2Asin/AsexVsSex/results.txt", sep="\t", header=TRUE, row.names=1, na.strings="NA")

# filter out transcripts <= 500 bp
exp <- Asex2Asin[(Asex2Asin$len > 500),]

info<-read.table("SampleInfo.txt", sep=" ", head=F)


colnames(info)<-c("code", "Sample", "Type")
exp2<-exp[grep("count", colnames(exp))]
#rownames(exp2) <- exp[,1]
colClean <- function(x){ colnames(x) <- gsub("\\.count", "", colnames(x)); x } 
exp2 <- colClean(exp2)
#transpose
exp3<-t(exp2)
head(info,2)
#add sample information
full<-merge(info, exp3, by.x="code", by.y="row.names")[,-1]
row.names(full)<-make.names(full[,1], unique=TRUE)
full<-full[,-1]
head(full[,1:5])

##########Separate tissue
final1<-as.data.frame(full[grep(tissue, rownames(full)),][,1])
colnames(final1)<-c("Type")
final2<-full[grep(tissue, rownames(full)),][,2:ncol(full)]


####Possible filters!!! Check that they are correct!!
#Keep only SexSpecies of interest
final2<-final2[grep(ancestral, rownames(final2)),]
#Remove outlier samples that we stored in vector "bad"
final2<-(subset(final2, !(rownames(final2) %in% bad)))
#(or use only specific list of samples)
#names<-c("Kaz_F_Gonad", "UrmAs_F_Gonad.1", "UrmSex_F_Gonad", "UrmAs_F_Gonad", "Kaz_F_Gonad.1")
#final2<-subset(final2, rownames(final2) %in% names)


#####################DESeq2 Analysis#############################
######sort so samples are in alphabetical order
final2<-final2[ order(row.names(final2)), ]
#transpose to right format
count_matrix<-t(final2)

######Make table with sample information
samples<-as.data.frame(colnames(count_matrix))
rownames(samples)<-colnames(count_matrix)
male<-as.data.frame(samples[grep("M", rownames(samples)),])
colnames(male)<-"Samplename"
male$Status<-"M"
female<-as.data.frame(samples[grep("F", rownames(samples)),])
colnames(female)<-"Samplename"
female$Status<-"F"
sampletable<-rbind(male, female)
rownames(sampletable)<-sampletable$Samplename
sampletable<-sampletable[ order(row.names(sampletable)), ]

########create the DESeq object
library("DESeq2")
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix, colData = sampletable, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)
resultsNames(dds) # lists the coefficients, so you know what to write for "coef=" in the next command

#####Standard differential expression analysis
sexbiased <- results(dds, name="Status_M_vs_F", pAdjustMethod = "BH", alpha=0.05)



#################### Asexuality Genes ###############################
final_asex<-full[grep(tissue, rownames(full)),][,2:ncol(full)]
#Keep only Species of interest (Akaz females & asexuals)
final_asex<-final_asex[grep(paste(ancestral,"_F|Apar|AurmAsex|Aata", sep=""), rownames(final_asex)),]
#Remove outlier samples that we stored in vector "bad"
final_asex<-(subset(final_asex, !(rownames(final_asex) %in% bad)))

######sort so samples are in alphabetical order
final_asex<-final_asex[ order(row.names(final_asex)), ]
#transpose to right format
count_matrix_asex<-t(final_asex)

######Make table with sample information
samples_asex<-as.data.frame(colnames(count_matrix_asex))
rownames(samples_asex)<-colnames(count_matrix_asex)
sexuals<-as.data.frame(samples_asex[grep(ancestral, rownames(samples_asex)),])
colnames(sexuals)<-"Samplename"
sexuals$Status<-"Sex"
asexuals<-as.data.frame(samples_asex[-grep(paste("^",ancestral, sep=""), rownames(samples_asex)),])
colnames(asexuals)<-"Samplename"
asexuals$Status<-"Asex"
sampletable_asex<-rbind(sexuals, asexuals)
sampletable_asex$Species <- as.factor(substring(sampletable_asex$Samplename, 0, regexpr("_", sampletable_asex$Samplename) -1))
rownames(sampletable_asex)<-sampletable_asex$Samplename
sampletable_asex<-sampletable_asex[ order(row.names(sampletable_asex)), ]

########create the DESeq object
library("DESeq2")
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data
se_star_matrix_asex <- DESeqDataSetFromMatrix(countData = count_matrix_asex, colData = sampletable_asex, design = ~ Status)

dds_asex <- DESeq(se_star_matrix_asex)
resultsNames(dds_asex) # lists the coefficients, so you know what to write for "coef=" in the next command

#####Standard differential expression analysis
asex_genes <- results(dds_asex, name="Status_Sex_vs_Asex", pAdjustMethod = "BH", alpha=0.05)

### Results with adjusted P-value cut-off
summary(asex_genes)

### Results with fold-change cut-off
resSign_asex <- subset(asex_genes, abs(log2FoldChange) > 1)
resOrdered_asex <- resSign_asex[order(resSign_asex$log2FoldChange),]
write.table(resOrdered_asex, file=paste("/home/ahuylman/1_Projects/Artemia/DifferentialGeneExpression/Asex2Asin/FC_cutoff/sign_DE-asex-vs-", ancestral, "-females_", tissue, ".txt", sep=""), quote=FALSE, sep="\t")


##### individual species comparisons
se_star_matrix_spec <- DESeqDataSetFromMatrix(countData = count_matrix_asex, colData=sampletable_asex, design=~Species)
dds_spec <- DESeq(se_star_matrix_spec)

### Results with adjusted P-value cut-off
res_Apar <- results(dds_spec, alpha=0.05, contrast=c("Species","Apar",ancestral))
res_Aata <- results(dds_spec, alpha=0.05, contrast=c("Species","Aata",ancestral))
res_Aurm <- results(dds_spec, alpha=0.05, contrast=c("Species","AurmAsex",ancestral))

### Results with fold-change cut-off
resSign_Apar <- subset(res_Apar, abs(log2FoldChange) > 1)
resOrdered_Apar <- resSign_Apar[order(resSign_Apar$log2FoldChange),]
write.table(resOrdered_Apar, file=paste("/home/ahuylman/1_Projects/Artemia/DifferentialGeneExpression/Asex2Asin/FC_cutoff/sign_DE-Apar-vs-",ancestral,"-females_", tissue, ".txt", sep=""), quote=FALSE, sep="\t")
resSign_Aata <- subset(res_Aata, abs(log2FoldChange) > 1)
resOrdered_Aata <- resSign_Aata[order(resSign_Aata$log2FoldChange),]
write.table(resOrdered_Aata, file=paste("/home/ahuylman/1_Projects/Artemia/DifferentialGeneExpression/Asex2Asin/FC_cutoff/sign_DE-Aata-vs-",ancestral,"-females_", tissue, ".txt", sep=""), quote=FALSE, sep="\t")
resSign_Aurm <- subset(res_Aurm, abs(log2FoldChange) > 1)
resOrdered_Aurm <- resSign_Aurm[order(resSign_Aurm$log2FoldChange),]
write.table(resOrdered_Aurm, file=paste("/home/ahuylman/1_Projects/Artemia/DifferentialGeneExpression/Asex2Asin/FC_cutoff/sign_DE-Aurm-vs-",ancestral,"-females_", tissue, ".txt", sep=""), quote=FALSE, sep="\t")




### 2. FPKM Table

##########Separate tissue
final1<-as.data.frame(full[grep(tissue, rownames(full)),][,1])
colnames(final1)<-c("Type")
final2<-full[grep(tissue, rownames(full)),][,2:ncol(full)]

####Possible filters!!! Check that they are correct!!
#Keep only female samples
final2<-final2[grep("F", rownames(final2)),]
#Remove outlier samples that we stored in vector "bad"
final2<-(subset(final2, !(rownames(final2) %in% bad)))
#(or use only specific list of samples)
#names<-c("Kaz_F_Gonad", "UrmAs_F_Gonad.1", "UrmSex_F_Gonad", "UrmAs_F_Gonad", "Kaz_F_Gonad.1")
#final2<-subset(final2, rownames(final2) %in% names)

#####sort so samples are in alphabetical order
final2<-final2[ order(row.names(final2)), ]
#transpose to right format
count_matrix<-t(final2)


######Make table with sample information
samples<-as.data.frame(colnames(count_matrix))
rownames(samples)<-colnames(count_matrix)
sex<-as.data.frame(samples[grep("Akaz|AurmSexual|Asin", rownames(samples)),])
colnames(sex)<-"Samplename"
sex$Status<-"S"
asex<-as.data.frame(samples[grep("Apar|Aata|AurmAsex", rownames(samples)),])
colnames(asex)<-"Samplename"
asex$Status<-"A"
sampletable<-rbind(sex, asex)
rownames(sampletable)<-sampletable$Samplename
sampletable<-sampletable[ order(row.names(sampletable)), ]

########create the DESeq object
library("DESeq2")
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix, colData = sampletable, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)

#Get FPKM
mcols(dds)$basepairs <-exp$len # the raw counts
fpkm<-as.data.frame(fpkm(dds))
head(fpkm)


#########Actual analysis
########################
ancestraltable<-(fpkm[grep(ancestral, colnames(fpkm))])
focaltable<-(fpkm[grep(focal, colnames(fpkm))])
temp<-cbind(ancestraltable, focaltable)
head(temp)

##########filter for expression accross samples
#filter out genes with any sample <1
temp<-(temp[rowSums(temp > 1) == ncol(temp), ])

#########Normalize
###Normalize
bolFMat<-as.matrix(temp, nrow = nrow(temp), ncol = ncol(temp))

library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
#temp2 <- normalize.quantiles(bolFMat)
rownames(temp2)<-rownames(temp)
colnames(temp2)<-colnames(temp)

#### Average replicates and calculate ratio
exp_final<-cbind(as.data.frame(rowMeans(temp2[, grep(ancestral, colnames(temp2))]))[,1], as.data.frame(rowMeans(temp2[, grep(focal, colnames(temp2))]))[,1])
rownames(exp_final)<-rownames(temp2)
colnames(exp_final)<-c("ancestral", "focal")
exp_final<-as.data.frame(exp_final)
exp_final$ratio<-log2(exp_final[,2]/exp_final[,1])

####Add sex-bias information and plot basic boxplot
exp_final<-merge(exp_final, as.data.frame(sexbiased)[,c(2,6)], by.x="row.names", by.y="row.names")
colnames(exp_final)<-c("gene", "ancestral", "focal", "ratio", "Log", "Qval")
exp_final$SBG[exp_final$Qval < 0.05 & exp_final$Log > 0] <- "MBG" 
exp_final$SBG[exp_final$Qval < 0.05 & exp_final$Log < 0] <- "FBG" 
exp_final$SBG[exp_final$Qval > 0.05] <- "UBG" 
exp_final$SBG <- as.factor(exp_final$SBG)
SBGs <- summary(exp_final$SBG)


########### Plotting ###############################
require(ggplot2)
library(ggsignif)

theme_set(theme_bw()+theme(panel.grid.major=element_blank(), panel.grid.minor=element_blank(), panel.border=element_rect(colour="black"), text=element_text(colour="black"), axis.title=element_text(size=16), legend.key=element_rect(colour="white"), legend.title = element_text(size=16), legend.text=element_text(size=16), axis.text.y=element_text(size=16), axis.text.x=element_text(size=16), legend.position="top", strip.background=element_rect(colour="black", fill="white"), strip.text=element_text(size=16, face="bold")))

boxplot <- ggplot(data=subset(exp_final, !is.na(SBG)), aes(x=SBG, y=ratio))+
geom_boxplot(aes(fill=SBG), notch=TRUE, outlier.colour=NA)+
scale_fill_manual(name=paste("SBGs in",ancestral), values=c("MBG"="#ace4acff", "FBG"="#669966", "UBG"="grey"))+
coord_cartesian(ylim=c(-4,+6))+
xlab("Gene Class")+
ylab(paste("log(",focal,"/",ancestral," female expression)", sep=""))+
scale_x_discrete(limits=c("FBG", "MBG", "UBG"), labels=c(paste("FBG\n(",SBGs["FBG"],")", sep=""), paste("MBG\n(",SBGs["MBG"],")", sep=""), paste("UBG\n(",SBGs["UBG"],")", sep="")))+ 
geom_hline(aes(yintercept=0), colour="grey", linetype="dashed")+
geom_signif(comparisons = list(c("FBG", "UBG")), map_signif_level=TRUE, test=wilcox.test, y_position=5.5, textsize = 10)+
geom_signif(comparisons = list(c("MBG", "UBG")), map_signif_level=TRUE, test=wilcox.test, y_position=4.5, textsize = 10)+
theme(legend.position="none", axis.title.x=element_blank())


svg(paste("Plots/",focal,"_over_",ancestral,"_",tissue,".svg", sep=""), width=3, height=5)
boxplot
dev.off()
```
