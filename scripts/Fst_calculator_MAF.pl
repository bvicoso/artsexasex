#!/usr/bin/perl

#inputed variables
my $vcf = $ARGV[0];
my $pop1 = $ARGV[1];
my $pop2 = $ARGV[2];
my $outname = $ARGV[3];

@pop1 = split (",", $pop1);
@pop2 = split (",", $pop2);

############open VCF and results file
open (INPUT, "$vcf"); 
open (RESULTS, ">$outname.maf.fst");

while ($line = <INPUT>) {
	next if($line  =~ /#/);

	@stuff=split(/\t/, $line);

	###calculate p1, q1
	$sumref1=0;
	$sumalt1=0;
	$p1=0;
	$q1=0;
	foreach (@pop1)
		{
		$coord=$_-1;
		$info = $stuff[$coord];
		@gtinfo = split(":", $info);
		@ad=split(",", $gtinfo[4]);
		$sumref1=$sumref1+$ad[0];
		$sumalt1=$sumalt1+$ad[1];
		}
	$p1=($sumref1/($sumref1+$sumalt1));
	$q1=($sumalt1/($sumref1+$sumalt1));


	###calculate p2, q2
	$sumref2=0;
	$sumalt2=0;
	$p2=0;
	$q2=0;
	foreach (@pop2)
		{
		$coord=$_-1;
		$info = $stuff[$coord];
		@gtinfo = split(":", $info);
		@ad=split(",", $gtinfo[4]);
		$sumref2=$sumref2+$ad[0];
		$sumalt2=$sumalt2+$ad[1];
		}
	$p2=($sumref2/($sumref2+$sumalt2));
	$q2=($sumalt2/($sumref2+$sumalt2));

	#keep only polymorphic sites
	if ($q1+$q2>0 && $p1+$p2>0 && (($p1+$p2)/2)>0.1 && (($q1+$q2)/2)>0.1)
	{
	$fst=(($p1-$p2)*($p1-$p2))/(($p1+$p2)*($q1+$q2));
	
	print RESULTS "$stuff[0] $stuff[1] $sumref1 $sumalt1 $p1 $q1 $sumref2 $sumalt2 $p2 $q2 $fst\n";
	}

}
