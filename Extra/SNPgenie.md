# Estimation of PiN, PiS, dN and dS from our RNA-seq data and transcriptome with SNPgenie

SNPgenie typically uses a reference genome and a GTF annotation file to estimate PiA and PiS. Here, the pipeline was adapted for a transcriptome and RNA-seq data. We first make a "pseudo-genome" from the transcripts, and the corresponding GTF file. Then we run SNP genie.

The input is:
* a set of CODING SEQUENCES in a fasta file. If you assembled a transcriptome de novo, make sure to first extract coding sequences using ORFfinder or the Evigene pipeline;
* RNA-seq reads

Programs/scripts required:
* SNPgenie
* MakeGTFcombined.pl ([link](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/Extra/MakeGTFcombined.pl))
* AdjustVCF.pl  ([link](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/Extra/AdjustVCF.pl))


## Make single fasta sequence

```
cat Asinica500.fa | grep -v '>' > Asinica_singleSeq.fa
sed '1i >fasta1' Asinica_singleSeq.fa > Asinica_singleSeq_head.fa
```

## Make the corresponding GTF file

```
perl MakeGTFcombined.pl Asinica500.fa

```


## Adjust VCF so it matches the "fake genome" gtf coordinates
In our case, we produced the VCF files following the steps shown in the [Fst analysis](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/Fst.md)

We now select the samples that we want, filter each of them, and run the "AdjustVCF.pl" tool to fix the coordinates to those of the pseudo-genome.

```
#load any modules here
module load vcftools
#VCFtools (0.1.15), April 1st 2020
module load bcftools
#Version: 1.8 (using htslib 1.8)

#Make_Fake_VCF.sh
srun bcftools view -s sin_39897.bam AllSamples.vcf.gz |  vcftools --vcf - --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 200 --minDP 10 --maxDP 200 --recode --stdout > sin_39897_filtered.vcf
srun perl AdjustVCF.pl Asinica500.fa sin_39897_filtered.vcf
```

(This will create the file sin_39897_filtered.vcf.fake ; do this for each sample to be analysed)


We need to set up the correct file structure, since SNPgenie always creates the same output directory! (WHY??)

```
mkdir SNPgenieHeads
mkdir SNPgenieHeads/sin_39897 SNPgenieHeads/sin_39898 SNPgenieHeads/urmsex_61775 SNPgenieHeads/urmsex_61779 
mv sin_39897_filtered.vcf.fake SNPgenieHeads/sin_39897/fake.vcf
mv sin_39898_filtered.vcf.fake SNPgenieHeads/sin_39898/fake.vcf
mv urmsex_61775_filtered.vcf.fake SNPgenieHeads/urmsex_61775/fake.vcf
mv urmsex_61779_filtered.vcf.fake SNPgenieHeads/urmsex_61779/fake.vcf
```

## SNPgenie

Run SNPgenie in each folder within SNPgenieHead (SNPgenie.sh)

```
srun /nfs/scistore03/vicosgrp/bvicoso/scripts/SNPGenie-master/snpgenie.pl --minfreq=0.01 --vcfformat=4 --snpreport=fake.vcf --fastafile=/nfs/scistore03/vicosgrp/bvicoso/Artemia_Popgen_Oct2019/2-Alignments/GetPi/Asinica_singleSeq_head.fa --gtffile=/nfs/scistore03/vicosgrp/bvicoso/Artemia_Popgen_Oct2019/2-Alignments/GetPi/Asinica500.fa.fake.gtf
```

## Processing of results

Let's now make a large file that contains only the PiA and PiS values for each sample. We first merge the results tables from the different samples:

```
#used paste command to column-bind all results files
#make sure that you are in the SNPgenieHeads/ folder
cd /path/to/SNPgenieHeads/
#combine all the files into a giant table
paste */SNPGenie_Results/product_results.txt
```

Then select only columns with ‘pi’ in their name. We first transpose the table so columns are now lines, then use grep to select lines that match the term "Pi", then transpose the table again - not the most straightforward way but I could not find a better one.

```
#transpose command from: http://archive.sysbio.harvard.edu/CSB/resources/computational/scriptome/UNIX/Tools/Change.html
cat Heads_SNPgenie.txt | perl -e ' $unequal=0; $_=<>; s/\r?\n//; @out_rows = split /\t/, $_; $num_out_rows = $#out_rows+1; while(<>) { s/\r?\n//; @F = split /\t/, $_; foreach $i (0 .. $#F) { $out_rows[$i] .= "\t$F[$i]"; } if ($num_out_rows != $#F+1) { $unequal=1; } } END { foreach $row (@out_rows) { print "$row\n" } warn "\nWARNING! Rows in input had different numbers of columns\n" if $unequal; warn "\nTransposed table: result has $. columns and $num_out_rows rows\n\n" } ' | grep 'pi'  | perl -e ' $unequal=0; $_=<>; s/\r?\n//; @out_rows = split /\t/, $_; $num_out_rows = $#out_rows+1; while(<>) { s/\r?\n//; @F = split /\t/, $_; foreach $i (0 .. $#F) { $out_rows[$i] .= "\t$F[$i]"; } if ($num_out_rows != $#F+1) { $unequal=1; } } END { foreach $row (@out_rows) { print "$row\n" } warn "\nWARNING! Rows in input had different numbers of columns\n" if $unequal; warn "\nTransposed table: result has $. columns and $num_out_rows rows\n\n" } ' > Heads_SNPgeniePiaPis.txt
```

Plot in R:

```
pi<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/PopGenOctober2019/Heads_SNPgeniePiaPis.txt", head=T, sep="\t")
pi<-subset(pi, piS_Aib_101424>0 & piS_Aib_101425>0 & piS_aib_40773>0 & piS_aib_40774>0 & piS_ata_45191>0 & piS_ata_45192>0 & piS_urmas_45056>0 & piS_urmas_45057>0 & piS_kaz_41809>0 & piS_kaz_41810>0 & piS_sin_39897>0 & piS_sin_39898>0 & piS_urmsex_61775>0 & piS_urmsex_61779>0)
boxplot(pi$piS_Aib_101424, pi$piS_Aib_101425, pi$piS_aib_40773, pi$piS_aib_40774, pi$piS_ata_45191, pi$piS_ata_45192, pi$piS_urmas_45056, pi$piS_urmas_45057, pi$piS_kaz_41809, pi$piS_kaz_41810, pi$piS_sin_39897, pi$piS_sin_39898, pi$piS_urmsex_61775, pi$piS_urmsex_61779, col=c("red", "red", "red", "red", "red", "red", "red", "red", "blue", "blue", "blue", "blue", "blue", "blue"), outline=F, notch=T)
#boxplot(pi$piN_Aib_101424/pi$piS_Aib_101424, pi$piN_Aib_101425/pi$piS_Aib_101425, pi$piN_aib_40773/pi$piS_aib_40773, pi$piN_aib_40774/pi$piS_aib_40774, pi$piN_ata_45191/pi$piS_ata_45191, pi$piN_ata_45192/pi$piS_ata_45192, pi$piN_urmas_45056/pi$piS_urmas_45056, pi$piN_urmas_45057/pi$piS_urmas_45057, pi$piN_kaz_41809/pi$piS_kaz_41809, pi$piN_kaz_41810/pi$piS_kaz_41810, pi$piN_sin_39897/pi$piS_sin_39897, pi$piN_sin_39898/pi$piS_sin_39898, pi$piN_urmsex_61775/pi$piS_urmsex_61775, pi$piN_urmsex_61779/pi$piS_urmsex_61779, col=c("red", "red", "red", "red", "red", "red", "red", "red", "blue", "blue", "blue", "blue", "blue", "blue"), outline=F, notch=T)

###same but exclude: pi$piS_aib_40773, pi$piS_aib_40774, pi$piS_ata_45191
pi<-read.table("~/Documents/Ina/ArtemiaSexAsex_oct2019/PopGenOctober2019/Heads_SNPgeniePiaPis.txt", head=T, sep="\t")
pi<-subset(pi, piS_Aib_101424>0 & piS_Aib_101425>0  & piS_ata_45192>0 & piS_urmas_45056>0 & piS_urmas_45057>0 & piS_kaz_41809>0 & piS_kaz_41810>0 & piS_sin_39897>0 & piS_sin_39898>0 & piS_urmsex_61775>0 & piS_urmsex_61779>0)
boxplot(pi$piS_Aib_101424, pi$piS_Aib_101425, pi$piS_ata_45192, pi$piS_urmas_45056, pi$piS_urmas_45057, pi$piS_kaz_41809, pi$piS_kaz_41810, pi$piS_sin_39897, pi$piS_sin_39898, pi$piS_urmsex_61775, pi$piS_urmsex_61779, col=c("red", "red", "red", "red", "red", "blue", "blue", "blue", "blue", "blue", "blue"), outline=F, notch=T)
boxplot(pi$piN_Aib_101424/pi$piS_Aib_101424, pi$piN_Aib_101425/pi$piS_Aib_101425, pi$piN_ata_45192/pi$piS_ata_45192, pi$piN_urmas_45056/pi$piS_urmas_45056, pi$piN_urmas_45057/pi$piS_urmas_45057, pi$piN_kaz_41809/pi$piS_kaz_41809, pi$piN_kaz_41810/pi$piS_kaz_41810, pi$piN_sin_39897/pi$piS_sin_39897, pi$piN_sin_39898/pi$piS_sin_39898, pi$piN_urmsex_61775/pi$piS_urmsex_61775, pi$piN_urmsex_61779/pi$piS_urmsex_61779, col=c("red", "red", "red", "red", "red", "blue", "blue", "blue", "blue", "blue", "blue"), outline=F, notch=T)
```
