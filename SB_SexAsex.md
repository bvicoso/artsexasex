# Call sex-biased genes, and compare their expression in sexual and asexuals

The R code below can be used to produce boxplots of changes in expression between lineages (Figure 3), as well as plotting the change as a function of ancestral expression, and reproducing boxplots only for the 10% of genes with the highest ancestral expression (to check that patterns hold independent of ancestral level of expression). A text version of the script is [here](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/scripts/SB_sexAsex.txt), and can be used in one go, after specifying the following parameters at the beginning:
* tissue
* "ancestral" species (used for sex-biased gene classification and as proxy for ancestral expression)
* focal "asexual" species (the species whose change in expression we are interested in).
* samples to be excluded, if any.

Similar code for plotting changes in male expression can be found [here](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/scripts/SB_MaleComparison.txt).

The required input files are:
* females_asex_vs_sex_results_new_filteredGreater500bp.txt, which contains read counts for each sample, is available [here](https://seafile.ist.ac.at/f/181d9e97950543ec87a6/?dl=1).
* SampleInfo.txt , which contains the sample information and can be found [here](https://git.ist.ac.at/bvicoso/artsexasex/-/blob/master/data/SampleInfo.txt).


NB: the script replaces the original sample codes with different names. To get the correspondence, type (after running the script):
```
full<-merge(info, exp3, by.x="code", by.y="row.names")
row.names(full)<-make.names(full[,2], unique=TRUE)
(full[,1:2])
```
<details>
  <summary>or click here for the correspondance!</summary>
  

New | original | info 
---|---|---
Aib_F_Head | X101424 | Aib_F_Head
Aib_F_Head.1 | X101425 | Aib_F_Head
Aib_F_Tho | X101426 | Aib_F_Tho
Aib_F_Tho.1 | X101427 | Aib_F_Tho
Aib_F_Gonad | X101428 | Aib_F_Gonad
Aib_F_Gonad.1 | X101429 | Aib_F_Gonad
Aib_F_WB | X101440 | Aib_F_WB
Aib_F_WB.1 | X101441 | Aib_F_WB
Sin_M_WB | X39869 | Sin_M_WB
Sin_M_WB.1 | X39870 | Sin_M_WB
Sin_F_WB | X39871 | Sin_F_WB
Sin_F_WB.1 | X39872 | Sin_F_WB
Kaz_M_WB | X39873 | Kaz_M_WB
Kaz_M_WB.1 | X39874 | Kaz_M_WB
Kaz_F_WB | X39875 | Kaz_F_WB
Kaz_F_WB.1 | X39876 | Kaz_F_WB
Sin_M_Head | X39877 | Sin_M_Head
Sin_M_Head.1 | X39878 | Sin_M_Head
Sin_M_Tho | X39879 | Sin_M_Tho
Sin_M_Tho.1 | X39880 | Sin_M_Tho
Sin_M_Gonad | X39895 | Sin_M_Gonad
Sin_M_Gonad.1 | X39896 | Sin_M_Gonad
Sin_F_Head | X39897 | Sin_F_Head
Sin_F_Head.1 | X39898 | Sin_F_Head
Sin_F_Gonad | X39899 | Sin_F_Gonad
Sin_F_Gonad.1 | X39900 | Sin_F_Gonad
Sin_M_Head.2 | X39901 | Sin_M_Head
Sin_M_Head.3 | X39902 | Sin_M_Head
Sin_M_Gonad.2 | X40767 | Sin_M_Gonad
Sin_M_Gonad.3 | X40768 | Sin_M_Gonad
Sin_F_Head.2 | X40769 | Sin_F_Head
Sin_F_Head.3 | X40770 | Sin_F_Head
Sin_F_Gonad.2 | X40771 | Sin_F_Gonad
Sin_F_Gonad.3 | X40772 | Sin_F_Gonad
Aib_F_Head.2 | X40773 | Aib_F_Head
Aib_F_Head.3 | X40774 | Aib_F_Head
Aib_F_Gonad.2 | X40775 | Aib_F_Gonad
Aib_F_Gonad.3 | X40776 | Aib_F_Gonad
Kaz_M_Head | X41803 | Kaz_M_Head
Kaz_M_Head.1 | X41804 | Kaz_M_Head
Kaz_M_Gonad | X41805 | Kaz_M_Gonad
Kaz_M_Gonad.1 | X41806 | Kaz_M_Gonad
Kaz_M_Tho | X41807 | Kaz_M_Tho
Kaz_M_Tho.1 | X41808 | Kaz_M_Tho
Kaz_F_Head | X41809 | Kaz_F_Head
Kaz_F_Head.1 | X41810 | Kaz_F_Head
Kaz_F_Tho | X41826 | Kaz_F_Tho
Kaz_F_Tho.1 | X41827 | Kaz_F_Tho
Kaz_F_Gonad | X41828 | Kaz_F_Gonad
Kaz_F_Gonad.1 | X41829 | Kaz_F_Gonad
Sin_F_Tho | X45052 | Sin_F_Tho
Sin_F_Tho.1 | X45053 | Sin_F_Tho
Aib_F_Tho.2 | X45054 | Aib_F_Tho
Aib_F_Tho.3 | X45055 | Aib_F_Tho
UrmAs_F_Head | X45056 | UrmAs_F_Head
UrmAs_F_Head.1 | X45057 | UrmAs_F_Head
UrmAs_F_Tho | X45058 | UrmAs_F_Tho
UrmAs_F_Tho.1 | X45059 | UrmAs_F_Tho
UrmAs_F_Gonad | X45060 | UrmAs_F_Gonad
UrmAs_F_Gonad.1 | X45061 | UrmAs_F_Gonad
Ata_F_Head | X45191 | Ata_F_Head
Ata_F_Head.1 | X45192 | Ata_F_Head
Ata_F_Tho | X45193 | Ata_F_Tho
Ata_F_Tho.1 | X45194 | Ata_F_Tho
Ata_F_Gonad | X45195 | Ata_F_Gonad
Ata_F_Gonad.1 | X45196 | Ata_F_Gonad
UrmSex_M_Head | X61773 | UrmSex_M_Head
UrmSex_M_Gonad | X61774 | UrmSex_M_Gonad
UrmSex_F_Head | X61775 | UrmSex_F_Head
UrmSex_F_Gonad | X61776 | UrmSex_F_Gonad
UrmSex_M_Head.1 | X61777 | UrmSex_M_Head
UrmSex_M_Gonad.1 | X61778 | UrmSex_M_Gonad
UrmSex_F_Head.1 | X61779 | UrmSex_F_Head
UrmSex_F_Gonad.1 | X61780 | UrmSex_F_Gonad


</details>


## 1. Call sex biased genes


Pick some parameters: tissue,  species to use for calling sex-biased genes (and as proxy for ancestral expression), and focal species (the asexual or control species whose expression we are interested in):

```
#R version 3.6.1 (2019-07-05)

##########Pick a few parameters
###Working directory
setwd("/Users/bvicoso/Documents/Ina/ArtemiaSexAsex_oct2019/submission/revision1")
###tissue to use (Head, Gonad or Tho)
tissue<-"Gonad"
### Pick pair of species to analyse
#Species for calling sex-biased genes (Sin, Kaz or UrmSex) and ancestral gene expression
ancestral<-"Sin"
#Now pick the focal species "asexual"
#Options: Sin, Kaz, UrmSex (urmiana sexual), Aib, Ata, UrmAs
focal<-"Aib"
###Samples to exclude
bad<-c("Aib_F_Gonad.3", "Aib_F_Gonad.2", "Aib_F_Head.3", "Aib_F_Head.2", "Aib_F_Tho.2", "Aib_F_Tho.3")

```

Load data:

```
#######load data and sample information
exp<-read.table("females_asex_vs_sex_results_new_filteredGreater500bp.txt", sep="\t", head=T)
info<-read.table("SampleInfo.txt", sep=" ", head=F)
colnames(info)<-c("code", "Sample", "Type")
exp2<-exp[grep("count", colnames(exp))]
rownames(exp2) <- exp[,1]
colClean <- function(x){ colnames(x) <- gsub("\\.count", "", colnames(x)); x } 
exp2 <- colClean(exp2)
#transpose
exp3<-t(exp2)
head(info,2)
#add sample information
full<-merge(info, exp3, by.x="code", by.y="row.names")[,-1]
row.names(full)<-make.names(full[,1], unique=TRUE)
full<-full[,-1]
head(full[,1:5])

##########Separate tissue
final1<-as.data.frame(full[grep(tissue, rownames(full)),][,1])
colnames(final1)<-c("Type")
final2<-full[grep(tissue, rownames(full)),][,2:ncol(full)]
```

Filter out bad samples (specified at the beginning):

```
####Possible filters!!! Check that they are correct!!
#Keep only SexSpecies of interest
final2<-final2[grep(ancestral, rownames(final2)),]
#Remove outlier samples that we stored in vector "bad"
final2<-(subset(final2, !(rownames(final2) %in% bad)))
#(or use only specific list of samples)
#names<-c("Kaz_F_Gonad", "UrmAs_F_Gonad.1", "UrmSex_F_Gonad", "UrmAs_F_Gonad", "Kaz_F_Gonad.1")
#final2<-subset(final2, rownames(final2) %in% names)
```

Run DESeq2 differential expression between males and females:

```
######sort so samples are in alphabetical order
final2<-final2[ order(row.names(final2)), ]
#transpose to right format
count_matrix<-t(final2)

######Make table with sample information
samples<-as.data.frame(colnames(count_matrix))
rownames(samples)<-colnames(count_matrix)
male<-as.data.frame(samples[grep("M", rownames(samples)),])
colnames(male)<-"Samplename"
male$Status<-"M"
female<-as.data.frame(samples[grep("F", rownames(samples)),])
colnames(female)<-"Samplename"
female$Status<-"F"
sampletable<-rbind(male, female)
rownames(sampletable)<-sampletable$Samplename
sampletable<-sampletable[ order(row.names(sampletable)), ]

########create the DESeq object
library("DESeq2")
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix, colData = sampletable, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)
resultsNames(dds) # lists the coefficients, so you know what to write for "coef=" in the next command

#####Standard differential expression analysis
sexbiased <- results(dds, name="Status_M_vs_F", pAdjustMethod = "BH", alpha=0.05)
```

## 2. Get FPKM table

First, let's make an FPKM table for **female samples**, after excluding bad samples (specified at the beginning). We have the data loaded so we can start at ##Separate tissue:

```
##########Separate tissue
final1<-as.data.frame(full[grep(tissue, rownames(full)),][,1])
colnames(final1)<-c("Type")
final2<-full[grep(tissue, rownames(full)),][,2:ncol(full)]

####Possible filters!!! Check that they are correct!!
#Keep only female samples
final2<-final2[grep("F", rownames(final2)),]
#Remove outlier samples that we stored in vector "bad"
final2<-(subset(final2, !(rownames(final2) %in% bad)))
#(or use only specific list of samples)
#names<-c("Kaz_F_Gonad", "UrmAs_F_Gonad.1", "UrmSex_F_Gonad", "UrmAs_F_Gonad", "Kaz_F_Gonad.1")
#final2<-subset(final2, rownames(final2) %in% names)
```

Create the sampletable and the DESeq2 object, and extract fpkm values:

```
#####sort so samples are in alphabetical order
final2<-final2[ order(row.names(final2)), ]
#transpose to right format
count_matrix<-t(final2)


######Make table with sample information
samples<-as.data.frame(colnames(count_matrix))
rownames(samples)<-colnames(count_matrix)
sex<-as.data.frame(samples[grep("Kaz|UrmSex|Sin", rownames(samples)),])
colnames(sex)<-"Samplename"
sex$Status<-"S"
asex<-as.data.frame(samples[grep("Aib|Ata|UrmAs", rownames(samples)),])
colnames(asex)<-"Samplename"
asex$Status<-"A"
sampletable<-rbind(sex, asex)
rownames(sampletable)<-sampletable$Samplename
sampletable<-sampletable[ order(row.names(sampletable)), ]

########create the DESeq object
library("DESeq2")
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix, colData = sampletable, design = ~ Status)

#########optional: filter out genes with a sum of counts below 10
#se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]
dds <- DESeq(se_star_matrix)

#Get FPKM
mcols(dds)$basepairs <-exp$len # the raw counts
fpkm<-as.data.frame(fpkm(dds))
head(fpkm)
```

## 3. Plot Log2(focal/ancestral) for sex-biased and unbiased genes

Select data from the ancestral (same as for sex-bias) and focal species, both of which were specified at the beginning, filter out low expression genes, and average replicates:

```
#########Actual analysis
########################
ancestraltable<-(fpkm[grep(ancestral, colnames(fpkm))])
focaltable<-(fpkm[grep(focal, colnames(fpkm))])
temp<-cbind(ancestraltable, focaltable)
head(temp)

##########filter for expression accross samples
#filter out genes with any sample <1
temp<-(temp[rowSums(temp > 1) == ncol(temp), ])

#########Normalize
###Normalize
bolFMat<-as.matrix(temp, nrow = nrow(temp), ncol = ncol(temp))
library(NormalyzerDE)
temp2<-performQuantileNormalization(bolFMat, noLogTransform = T)
rownames(temp2)<-rownames(temp)
colnames(temp2)<-colnames(temp)

#### Average replicates and calculate ratio
exp_final<-cbind(as.data.frame(rowMeans(temp2[, grep(ancestral, colnames(temp2))]))[,1], as.data.frame(rowMeans(temp2[, grep(focal, colnames(temp2))]))[,1])
rownames(exp_final)<-rownames(temp2)
colnames(exp_final)<-c("ancestral", "focal")
exp_final<-as.data.frame(exp_final)
exp_final$ratio<-log2(exp_final[,2]/exp_final[,1])
```

Plot the change between focal and ancestral for ancestrally unbiased, female- and male-biased genes:

```
####Add sex-bias information and plot boxplot
exp_final<-merge(exp_final, as.data.frame(sexbiased)[,c(2,6)], by.x="row.names", by.y="row.names")
colnames(exp_final)<-c("gene", "ancestral", "focal", "ratio", "Log", "Qval")
exp_final$SBG[exp_final$Qval < 0.05 & exp_final$Log > 0] <- "MBG" 
exp_final$SBG[exp_final$Qval < 0.05 & exp_final$Log < 0] <- "FBG" 
exp_final$SBG[exp_final$Qval > 0.05] <- "UBG" 
exp_final$SBG <- as.factor(exp_final$SBG)
SBGs <- summary(exp_final$SBG)

require(ggplot2)
library(ggsignif)

theme_set(theme_bw()+theme(panel.grid.major=element_blank(), panel.grid.minor=element_blank(), panel.border=element_rect(colour="black"), text=element_text(colour="black"), axis.title=element_text(size=16), legend.key=element_rect(colour="white"), legend.title = element_text(size=16), legend.text=element_text(size=16), axis.text.y=element_text(size=16), axis.text.x=element_text(size=16), legend.position="top", strip.background=element_rect(colour="black", fill="white"), strip.text=element_text(size=16, face="bold")))

test <- ggplot(data=subset(exp_final, !is.na(SBG)), aes(x=SBG, y=ratio))+
geom_boxplot(aes(fill=SBG), notch=TRUE, outlier.colour=NA)+
scale_fill_manual(name=paste("SBGs in",ancestral), values=c("MBG"="#ace4acff", "FBG"="#669966", "UBG"="grey"))+
coord_cartesian(ylim=c(-6,+6))+
xlab("Gene Class")+
ylab(paste("log(",focal,"/",ancestral," female expression)", sep=""))+
scale_x_discrete(limits=c("FBG", "MBG", "UBG"), labels=c(paste("FBG\n(N=",SBGs["FBG"],")", sep=""), paste("MBG\n(N=",SBGs["MBG"],")", sep=""), paste("UBG\n(N=",SBGs["UBG"],")", sep="")))+ 
geom_hline(aes(yintercept=0), colour="grey", linetype="dashed")+
geom_signif(comparisons = list(c("FBG", "UBG")), map_signif_level=TRUE, test=wilcox.test, y_position=5.5, textsize = 10)+
geom_signif(comparisons = list(c("MBG", "UBG")), map_signif_level=TRUE, test=wilcox.test, y_position=4.5, textsize = 10)
```

## 4. Plot Log2(focal/ancestral) as a function of ancestral expression to test for regression to mean

We can further plot the change in focal versus ancestral as a function of ancestral expression, to see if the shifts we observe are simply due to female-biased genes having unusually high ancestral female-expression.  We also produce a boxplot using only the 25% fo genes with highest ancestral expression. 

```
### Plot as a function of ancestral female expression
dev.new(width=6, height=3)
par(mar=c(4,4,2,2), mfrow=c(1,2))

#make fat dotplot and skinny boxplot
layout(matrix(c(1,1,2), 1, 3, byrow = TRUE))

#let's create name
finalname<-paste("Ancestral = ", ancestral, " | Focal =", focal)
plot(log2(ub_final$ancestral), ub_final$ratio, xlab="Log2(Ancestral_F)", ylab="Log2(F/ancestral_F)", col="grey", main=finalname, cex.lab=1.5, cex.axis=1.5, cex.main=1.5)
cor.test(ub_final$ancestral, ub_final$ratio)
points(mb_final$ancestral, mb_final$ratio, col="#C7E9C0", pch=20)
points(fb_final$ancestral, fb_final$ratio, col="#006D2C", pch=20)
lines(lowess(log2(ub_final$ancestral), (ub_final$ratio)), type="l", col="black", lwd=2)
lines(lowess(log2(fb_final$ancestral), (fb_final$ratio)), type="l", col="#006D2C", lwd=4)
lines(lowess(log2(mb_final$ancestral), (mb_final$ratio)), type="l", col="#74C476", lwd=4)

#plot boxplot for high expression genes

#get limit
high<-quantile(exp_final$ancestral, 0.75)
high
mbh<-subset(mb_final, ancestral>high)
fbh<-subset(fb_final, ancestral>high)
ubh<-subset(ub_final, ancestral>high)

#check boxplot results
par(mar=c(4,2,2,2))
boxplot(fbh$ratio, mbh$ratio,  ubh$ratio, col=c("#006D2C", "#C7E9C0", "grey"), names=c("FB", "MB", "UB"), notch=T, outline=F, cex.axis=1.5, whiskcol="grey", staplecol="grey")

 if (wilcox.test(fbh$ratio, ubh$ratio)$p.value<0.05){
text(1, quantile(fbh$ratio, 0.90), format(wilcox.test(fbh$ratio, ubh$ratio)$p.value, digits=1), cex=1.5)
}

if (wilcox.test(mbh$ratio, ubh$ratio)$p.value<0.05){
text(2, quantile(mbh$ratio, 0.90), format(wilcox.test(mbh$ratio, ubh$ratio)$p.value, digits=1), cex=1.5)
}
```


